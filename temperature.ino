#include <OneWire.h> //https://github.com/PaulStoffregen/OneWire
#include <DallasTemperature.h> //https://github.com/milesburton/Arduino-Temperature-Control-Library

#define ANALOG_TEMP A5
#define DIGITAL_TEMP A4

OneWire oneWire(A4);
DallasTemperature sensors(&oneWire);
 
void setup(){
  Serial.begin(9600);
  sensors.begin();
}
 
void loop(){
  float analogTemperature = getAnalogTemperature();
  float digitalTemperature = getDigitalTemperature();
 
  Serial.print("Analog temperature: ");
  Serial.print(analogTemperature);
  Serial.print("*C");
  
  Serial.print(" Digital temperature: ");
  Serial.print(digitalTemperature);
  Serial.println("*C");

  delay(2000);
}

float getAnalogTemperature() {
  float voltage = (analogRead(ANALOG_TEMP) * 5.0) / 1024.0;
  return (voltage - 0.5) * 100;
}

float getDigitalTemperature() {
  sensors.requestTemperatures();
  return sensors.getTempCByIndex(0);
}
